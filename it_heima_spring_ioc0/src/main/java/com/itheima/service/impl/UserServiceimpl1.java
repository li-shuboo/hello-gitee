package com.itheima.service.impl;

import com.itheima.dao.UserDao;
import com.itheima.dao.UserDao1;
import com.itheima.service.UserService;
import com.itheima.service.UserService1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

//@Component("userServic1")
@Service("userServic1")
public class UserServiceimpl1 implements UserService1 {

    //    @Resource(name = "userDao1")相当于@Autowired + @Qualifier
    @Autowired
    @Qualifier("userDao1")//按照名称匹配，需配合Autowried使用，不加则按类型自动匹配
    private UserDao1 userDao1;
    @Value("${jdbc.driver}")
    private String driver;

    public void setUserDao(UserDao1 userDao1) {
        this.userDao1 = userDao1;
    }

    public UserServiceimpl1() {
    }

    public UserServiceimpl1(UserDao1 userDao1) {
        this.userDao1 = userDao1;
    }


    public void save() {
//        ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
//        UserDao userDao = (UserDao) app.getBean("userDao");
        System.out.println(driver);
        userDao1.save();
    }
}
