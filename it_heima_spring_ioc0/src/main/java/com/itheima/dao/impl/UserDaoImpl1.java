package com.itheima.dao.impl;

import com.itheima.dao.UserDao;
import com.itheima.dao.UserDao1;
import com.itheima.domain.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Properties;

//注解配置
@Component("userDao1")
public class UserDaoImpl1 implements UserDao1 {

    private List<String> strlist;
    private Map<String, User> userMap;
    private Properties properties;

    public void setStrlist(List<String> strlist) {
        this.strlist = strlist;
    }

    public void setUserMap(Map<String, User> userMap) {
        this.userMap = userMap;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    private String username;
    private int age;

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void save() {
        System.out.println(strlist);
        System.out.println(userMap);
        System.out.println(properties);
        System.out.println(username + "====" + age);
        System.out.println("save running...");
    }
}
