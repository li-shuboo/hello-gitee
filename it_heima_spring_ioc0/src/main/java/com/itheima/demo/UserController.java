package com.itheima.demo;

import com.itheima.cofig.SpringCofiguration;
import com.itheima.dao.UserDao;
import com.itheima.service.UserService;
import com.itheima.service.UserService1;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.lang.reflect.AnnotatedElement;

public class UserController {
    public static void main(String[] args) {
//        ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(SpringCofiguration.class);
        UserService1 userService1 = (UserService1) app.getBean(UserService1.class);
        userService1.save();

    }
}
