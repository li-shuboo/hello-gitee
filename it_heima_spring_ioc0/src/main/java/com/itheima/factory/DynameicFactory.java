package com.itheima.factory;

import com.itheima.dao.UserDao;
import com.itheima.dao.impl.UserDaoImpl;

public class DynameicFactory {
    public UserDao getUserDao(){
        return new UserDaoImpl();
    }
}
